import pandas as pd
from pandas import DataFrame
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt

# while True:
#     print("\nEnter file path of data set: ")
#     set_path = input()
#     try:
#         test = open(set_path, 'r').read()
#         if test:
#             break
#     except IOError:
#         print("There isn't such a file, try entering full path again.")


def main():
    set_path = 'failure.csv'

    pd.options.display.float_format = '{:,.2f}'.format
    df = pd.read_csv(set_path)

    pd.set_option('display.max_columns', None)
    pd.set_option('expand_frame_repr', False)

    target = df.DEATH_EVENT
    inputs = df.drop('DEATH_EVENT', axis='columns')

    x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.2)

    model = GaussianNB()

    # run_tests(target, inputs, model)

    model.fit(x_train, y_train)
    print("\nTraining score : {:0.2f}".format(model.score(x_test, y_test) * 100), "%\n")

    predictions = model.predict(x_test)
    percentages = model.predict_proba(x_test)

    chances = DataFrame(percentages)

    alive = chances.iloc[:, 0] * 100
    alive_list = alive.values.tolist()
    dead = chances.iloc[:, 1] * 100
    dead_list = dead.values.tolist()

    values = DataFrame(x_test)

    values['% alive'] = alive_list
    values['% dead'] = dead_list
    values['Will die?'] = predictions

    values['DEATH_EVENT'] = df['DEATH_EVENT']

    print("Chart with my predictions\n", values.sort_index(ascending=True))


def run_tests(target, inputs, model):
    score_10_90 = 0
    score_20_80 = 0
    score_30_70 = 0
    score_40_60 = 0
    score_50_50 = 0
    score_60_40 = 0
    score_70_30 = 0
    score_80_20 = 0
    score_90_10 = 0

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.9)
        model.fit(x_train, y_train)
        score_10_90 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.8)
        model.fit(x_train, y_train)
        score_20_80 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.7)
        model.fit(x_train, y_train)
        score_30_70 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.6)
        model.fit(x_train, y_train)
        score_40_60 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.5)
        model.fit(x_train, y_train)
        score_50_50 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.4)
        model.fit(x_train, y_train)
        score_60_40 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.3)
        model.fit(x_train, y_train)
        score_70_30 += model.score(x_test, y_test)

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.2)
        model.fit(x_train, y_train)
        score_80_20 += model.score(x_test, y_test)
        # print(model.score(x_test, y_test))

    for x in range(1000):
        x_train, x_test, y_train, y_test = train_test_split(inputs, target, test_size=0.1)
        model.fit(x_train, y_train)
        score_90_10 += model.score(x_test, y_test)

    print("\nTraining score with 10/90 split: {:0.5f}".format(score_10_90 / 1000 * 100))
    print("\nTraining score with 20/80 split: {:0.5f}".format(score_20_80 / 1000 * 100))
    print("\nTraining score with 30/70 split: {:0.5f}".format(score_30_70 / 1000 * 100))
    print("\nTraining score with 40/60 split: {:0.5f}".format(score_40_60 / 1000 * 100))
    print("\nTraining score with 50/50 split: {:0.5f}".format(score_50_50 / 1000 * 100))
    print("\nTraining score with 60/40 split: {:0.5f}".format(score_60_40 / 1000 * 100))
    print("\nTraining score with 70/30 split: {:0.5f}".format(score_70_30 / 1000 * 100))
    print("\nTraining score with 80/20 split: {:0.5f}".format(score_80_20 / 1000 * 100))
    print("\nTraining score with 90/10 split: {:0.5f}".format(score_90_10 / 1000 * 100))

    xs = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    ys = [score_10_90/1000*100, score_20_80/1000*100, score_30_70/1000*100, score_40_60/1000*100, score_50_50/1000*100, score_60_40/1000*100, score_70_30/1000*100, score_80_20/1000*100, score_90_10/1000*100]

    plt.plot(xs, ys, color='green', linestyle='dashed', linewidth=3, marker='o', markerfacecolor='blue', markersize=12)

    plt.xlabel('Amount of data used to train %')
    plt.ylabel('Accuracy %')

    plt.title('Accuracy plot')

    for x, y in zip(xs, ys):
        label = "{:.2f}".format(y)

        plt.annotate(label,
                     (x, y),
                     textcoords="offset points",
                     xytext=(0, 10),
                     ha='center')

    plt.show()


if __name__ == "__main__":
    main()
